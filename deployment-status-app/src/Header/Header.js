import React, { Component } from "react";
import './Header.css';

class Header extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return (
			<div className="navbar">
				<a href="/home">Home</a>
				<a href="/news">General News</a>
				<a href="/getdeploymentstatus">Deployment Status</a>
				<div className="dropdown">
					<button className="dropbtn">
						Covid-19
						<i className="fa fa-caret-down"></i>
					</button>
					<div className="dropdown-content">
						<a href="/mobiles">Mobiles</a>
						<a href="/tablets">Tablets</a>
						<a href="/tv">Televisions</a>
					</div>
				</div>
			</div>
		);
	}
}

export default Header;
