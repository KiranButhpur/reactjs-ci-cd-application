import React, { Component } from "react";
import "./App.css";
import Header from "./Header/Header";
import Routes from "./RoutesConfiguration";
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Header />
        </header>
        <Routes />
        <footer className="App-footer footer-position">
          <h5>This is space for Footer</h5>
        </footer>
      </div>
    );
  }
}

export default App;
