import React, { Component } from "react";
import "./News.css";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import axios from "axios";

class News extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	componentDidMount() {
		this.invokeNewsService();
	}

	invokeNewsService() {
		axios
			.get(
				"http://newsapi.org/v2/top-headlines?country=us&apiKey=d565844315db4c5f8c173dc44bb7658f"
			)
			.then(response => {
				//console.log(response);
				this.setState({ newsData: response });
			});
	}

	viewArticleInDetail = (e, articleUrl) => {
		e.preventDefault();
		console.log("Article URL : " + articleUrl);
	};

	render() {
		if (!this.state.newsData)
			return <p>You should soon be able to see the news.... Relax</p>;
		return (
			<div>
				<div className="row">
					{this.state.newsData.data.articles.map(
						(articleData, index) => (
							<Card className="article-card" key={index}>
								<Card.Img
									variant="top"
									className="article-card-image"
									src={articleData.urlToImage}
								/>
								<Card.Body className="article-card-body">
									<Card.Title className="article-card-title">
										<a
											href={articleData.url}
											target="empty"
										>
											{`${articleData.title.substring(
												0,
												45
											)}...`}
										</a>
									</Card.Title>
									<Card.Text className="article-card-description">
										{`${articleData.description.substring(
											0,
											80
										)}...`}
									</Card.Text>
									<div className="article-card-button">
										<Button
											variant="primary"
											onClick={e =>
												this.viewArticleInDetail(
													e,
													articleData.url
												)
											}
										>
											View More...
										</Button>
									</div>
								</Card.Body>
							</Card>
						)
					)}
				</div>
			</div>
		);
	}
}

export default News;
