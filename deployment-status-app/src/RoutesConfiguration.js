import React, {Component} from "react";
import DeploymentStatus from "./DeploymentStatus";
import News from "./News/News";
import {
  Switch,
  Route
} from "react-router-dom";

class Routes extends Component {

	render() {
		return (
		<div>
		<Switch>
            <Route
              exact
              path="/getdeploymentstatus"
              component={DeploymentStatus}
            />
            <Route
              exact
              path="/news"
              component={News}
            />
          </Switch>
          </div>
			);
		};
}

export default Routes;
