import React, { Component } from "react";
import axios from "axios";

class DeploymentStatus extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	//function which is called the first time the component loads
	componentDidMount() {
		this.invokeServiceAPI();
	}

	//Function to get the API response from mock (or) API Call
	invokeServiceAPI() {
		axios.get("mocks/deployment_version.json").then(response => {
			this.setState({ deploymentstatus: response });
		});
	}
	render() {
		if (!this.state.deploymentstatus)
			return (
				<p>You should soon be able to see the code status.... Relax</p>
			);
		return (
			<div>
				{this.state.deploymentstatus.data.map((displayDeplStatus, index) => (
					<div key={index}>
						<div
							style={{
								display: "grid",
								gridTemplateColumns: "repeat(2, 1fr)",
								gridGap: 20
							}}
						>
							<div className="leavemargin">
								<span className = "title-text">Staging{" "}</span>
								<div
									className={
										"leavemargin" +
										(displayDeplStatus.staging.version ===
										displayDeplStatus.production.version
											? " black-font"
											: " red-font")
									}
								>
									{displayDeplStatus.staging.version}
								</div>
							</div>
							<div className="leavemargin ">
								<span className = "title-text">Production{" "}</span>
								<div
									className={
										"leavemargin" +
										(displayDeplStatus.staging.version ===
										displayDeplStatus.production.version
											? " black-font"
											: " red-font")
									}
								>
									{displayDeplStatus.production.version}
								</div>
							</div>
						</div>
					</div>
				))}
			</div>
		);
	}
}

export default DeploymentStatus;
