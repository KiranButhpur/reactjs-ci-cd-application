This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify


*******************************
CI CD Application Requirement : 
*******************************

Create web page that should contain two columns
      i.  current deployment version (Constantly curl from server to find version number)
      ii. what deployment version I'm planning to deploy (if it doesnt match then it should say in red-color)
____________________

Code Breakdown:
____________________

IDE : Sublime Text

Formatter : JSPrettify for Sublime Text IDE

Create a project using below commands
	npm install -g create-react-app
	create-react-app deployment-status-app
	cd deployment-status-app/
	npm start
App.js would be the entry point for the application
Header, Footer and all global components displayed as part of Layout/Template should get registered in App.js
To redirect to the page(ideally Home Page) but in our case it will be Deployment Status page, we need to Redirect to the page using Router. Router can be implemented using React-Router module
	npm install --save react-router
Create a JS file DeploymentStatus.js and add a constructor to register the state object.
Create a ComponentDidMount method that will get executed before your render method of the component gets executed.
 	Invoke the REST API in the above method (Certainly, there needs a replace for mock invocation with your Service API call)
 	npm install axios
 	Now, the response should be bind to the object of your class. So, use setState()
 	In render method, all the HTML should go into the return call.
 	Use map function to iterate the array and get the values.
 	CSS Styling for the required cosmetics. All the CSS styling would be in App.js. Perhaps, this should be segregated to the respective component implementation.
Once downloaded the app, 
	npm install – Installs NodeModules
	npm start
A browser should open the url http://localhost:3000 and the page should be displayed as below(The text will be displayed in RED and BLINKS if the versions do not match. Else if version match, then the font would be displayed in BLACK color.)

Make any changes required.
Once satisfied, do
 	npm run build – This creates production version.(minified artefacts)
